libparse-mediawikidump-perl (1.0.6-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 14:34:38 +0100

libparse-mediawikidump-perl (1.0.6-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Remove Xavier Oswald from Uploaders. (Closes: #824319)
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:40:54 +0100

libparse-mediawikidump-perl (1.0.6-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:08:28 +0200

libparse-mediawikidump-perl (1.0.6-2) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Angel Abad ]
  * Email change: Angel Abad -> angel@debian.org

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Mark package as autopkg-testable
  * Declare conformance with Policy 3.9.7
  * Bump debhelper compatibility level to 9
  * Add pod-errors.patch fixing a minor POD error

 -- Damyan Ivanov <dmn@debian.org>  Mon, 22 Feb 2016 09:30:10 +0000

libparse-mediawikidump-perl (1.0.6-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Angel Abad ]
  * New upstream release
  * Bump Standards-Version to 3.9.1 (no changes)
  * Switch to dpkg-source format 3.0 (quilt)

  [ gregor herrmann ]
  * Add NEWS.Debian explaining that this modules is being retired.
  * debian/control: remove versions from (build) dependencies that are already
    satisfied in lenny.

 -- Angel Abad <angelabad@gmail.com>  Thu, 09 Dec 2010 00:56:22 +0100

libparse-mediawikidump-perl (1.0.4-1) unstable; urgency=low

  [ Jonathan Yu ]
  * Adds Test::Warn for testing

  [ Angel Abad ]
  * New upstream release
  * debian/copyright: Update
  * debian/control: Update B-D and B-D-I

 -- Angel Abad <angelabad@gmail.com>  Tue, 05 Jan 2010 13:59:41 +0100

libparse-mediawikidump-perl (1.0.1-1) unstable; urgency=low

  * New upstream release
  * Update dependencies per upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 13 Nov 2009 15:48:52 -0500

libparse-mediawikidump-perl (0.98-1) unstable; urgency=low

  * New upstream release
  * Update dependencies per upstream

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 29 Oct 2009 05:57:22 -0400

libparse-mediawikidump-perl (0.97-1) unstable; urgency=low

  [ Angel Abad ]
  * Update my email address
  * New upstream release 0.96

  [ gregor herrmann ]
  * New upstream release 0.97.
  * Remove (build) dependency on libobject-destroyer-perl.

 -- gregor herrmann <gregoa@debian.org>  Fri, 23 Oct 2009 22:25:02 +0200

libparse-mediawikidump-perl (0.95-1) unstable; urgency=low

  * New upstream release
  * Lists copyright holders in chronologically reverse order

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 15 Oct 2009 18:35:34 -0400

libparse-mediawikidump-perl (0.94-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Wed, 30 Sep 2009 11:23:16 +0200

libparse-mediawikidump-perl (0.93-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
    + Pages is now a subclass of Revisions
    + Fix a memory leak in Pages and Revisions
    + Now uses Test::Exception
  * Changed to new debhelper format
  * Updated copyright information
  * Added myself to Uploaders and Copyright
  * Fixed dependencies and updated control description

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ryan Niebur ]
  * Update jawnsy's email address

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 20 Sep 2009 11:37:41 -0400

libparse-mediawikidump-perl (0.91-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    + remove DM-Upload-Allowed
    + Set Standards-Version to 3.8.1
    + Change my mail address
  * Update debian/copyright
  * Remove libparse-mediawikidump-perl.docs, no more needed

 -- Xavier Oswald <xoswald@debian.org>  Thu, 14 May 2009 21:11:18 +0200

libparse-mediawikidump-perl (0.55-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza); switched XS-DM-Upload-Allowed to DM-Upload-Allowed.

  [ Ansgar Burchardt ]
  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 04 Jan 2009 13:57:40 +0100

libparse-mediawikidump-perl (0.53-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Fri, 14 Nov 2008 19:30:06 +0100

libparse-mediawikidump-perl (0.52-1) unstable; urgency=low

  * New upstream release.
  * Add myself to Uploaders.
  * debian/control: Mention module name in description
  * Refresh debian/rules for debhelper 7
  * Convert debian/copyright to proposed machine-readable format

 -- Ansgar Burchardt <ansgar@43-1.org>  Thu, 02 Oct 2008 00:19:21 +0200

libparse-mediawikidump-perl (0.51-1) unstable; urgency=low

  * New upstream release.
  * debian/copyright: add upstream source location, remove dh-make-perl
    boilerplate, copy author and copyright information verbatim from source.
  * Set Standards-Version to 3.8.0 (no changes).
  * Add /me to Uploaders.
  * Refresh debian/rules. Don't install README; install new TODO and new
    examples.

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Jun 2008 23:21:11 +0200

libparse-mediawikidump-perl (0.40-5) unstable; urgency=low

  * New maintainer is Debian perl group
  * Fix FTBFS with Perl 5.10 (closes: #467999).
  * Bumped Standards-Version to 3.7.3 (no changes required).
  * Bumped debhelper compatibility level to 6.
  * Made debian/copyright more explicit.
  * debian/watch: use dist-based URL.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields. Add XS-DM-Upload-Allowed: yes.

 -- Xavier Oswald <x.oswald@free.fr>  Wed, 27 Feb 2008 23:07:43 +0100

libparse-mediawikidump-perl (0.40-4) unstable; urgency=low

  * Upgraded Standards-Version to 3.7.2 (no changes required).

 -- Roland Mas <lolando@debian.org>  Wed, 05 Sep 2007 19:30:20 +0200

libparse-mediawikidump-perl (0.40-3) unstable; urgency=low

  * Highlighted the fact that this package only concerns the XML dumps,
    not the SQL dumps (closes: #386013).

 -- Roland Mas <lolando@debian.org>  Wed, 05 Sep 2007 19:18:57 +0200

libparse-mediawikidump-perl (0.40-2) unstable; urgency=low

  * Added watch file.

 -- Roland Mas <lolando@debian.org>  Sun, 30 Jul 2006 22:15:25 +0200

libparse-mediawikidump-perl (0.40-1) unstable; urgency=low

  * New upstream release.

 -- Roland Mas <lolando@debian.org>  Fri, 28 Jul 2006 20:48:54 +0200

libparse-mediawikidump-perl (0.30-3) unstable; urgency=low

  * Added a build-dependency on libxml-parser-perl (closes: #351815).
  * Also removed unneeded files from source package.

 -- Roland Mas <lolando@debian.org>  Tue,  7 Feb 2006 19:44:10 +0100

libparse-mediawikidump-perl (0.30-2) unstable; urgency=low

  * Fixed a few lintian warnings and errors.

 -- Roland Mas <lolando@debian.org>  Sat, 14 Jan 2006 13:58:55 +0100

libparse-mediawikidump-perl (0.30-1) unstable; urgency=low

  * Initial Release, almost straight from dh-make-perl.

 -- Roland Mas <lolando@debian.org>  Thu, 12 Jan 2006 15:24:08 +0100
